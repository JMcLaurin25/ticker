
#include "input.h"

#include <stdlib.h>
#include <string.h>

void get_adjustments(market *mkt)
{
	size_t sz, orig_sz;
	sz = orig_sz = 82;
	char *user_buf = malloc(sz);
	if (!user_buf) {
		return;
	}
	memset(user_buf, '\0', sz);

	while (getline(&user_buf, &sz, stdin) > 0) {
		if (strnlen(user_buf, sz) < orig_sz) {
			market_adjust(mkt, user_buf);
		} 

		memset(user_buf, '\0', strlen(user_buf));
	}
	free(user_buf);
}
