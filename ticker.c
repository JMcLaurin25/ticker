#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "input.h"
#include "stock.h"
#include "tree.h"

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf(" - Please Provide Market Seed Data.\n");
		return 1;
	}

	FILE *seed_file;
	seed_file = fopen(argv[1], "r");
	if (!seed_file) {
		printf("Error reading seed file.\n");
		return 1;
	}

	market *name_mkt = market_create(&cmp_name);
	market *price_mkt = market_create(&cmp_price);

	seed_market(name_mkt, seed_file);

	get_adjustments(name_mkt);

/*	Create price organized tree from name organized tree*/
	build_price_mkt(price_mkt, name_mkt);

	print_mkt_data(price_mkt);

/*	Clean-up*/
	market_destroy(name_mkt, &tree_disassemble);
	market_destroy(price_mkt, &tree_destroy);

	fclose(seed_file);
}
