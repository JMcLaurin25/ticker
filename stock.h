
#ifndef STOCK_H
#define STOCK_H

#include <stdio.h>
#include <stdbool.h>

struct company {
	char symbol[6];
	size_t cents;
	char *name;
};

struct company *stock_create(char *symbol, char *name, double price);

void stock_destroy(struct company *);

//Stock comparator functions
int cmp_price(const struct company *, const struct company *);
int cmp_name(const struct company *, const struct company *);

#endif
