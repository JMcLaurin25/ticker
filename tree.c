
#include "tree.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

/*Create the new market tree, with comparator function type*/
market *market_create(int (*cmp)(const struct company *, const struct company *))
{
	market *mkt = malloc(sizeof(*mkt));
	if(mkt) {
		mkt->root = NULL;
		mkt->cmp = cmp;
	}

	return mkt;
}

static int height(struct tree *t)
{
    if (!t)
        return 0;
    return t->height;
}

static int get_balance(struct tree *t)
{
    if (!t)
        return 0;

    return height(t->right) - height(t->left);
}

/*Rotate to the right*/
static struct tree *rot_right(struct tree *t)
{
	if (!t) {
		return NULL;
	}

    struct tree *old_left = t->left;
    struct tree *tmp = old_left->right;

/*	Rotate pointers*/
    old_left->right = t;
    t->left = tmp;

/*	Height update*/
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_left->height = (height(old_left->left) > height(old_left->right) ? height(old_left->left) : height(old_left->right)) + 1;

/*	Reset the root*/
    return old_left;
}

/*Rotate to the left*/
static struct tree *rot_left(struct tree *t)
{
 	if (!t) {
		return NULL;
	}

	struct tree *old_right = t->right;
    struct tree *tmp = old_right->left;
 
/*	Rotates pointers*/
    old_right->left = t;
    t->right = tmp;
 
/*	Height update*/
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_right->height = (height(old_right->left) > height(old_right->right) ? height(old_right->left) : height(old_right->right)) + 1;
 
/*	Reset the root*/
    return old_right;
}

static struct tree *tree_create(struct company *stock)
{
	struct tree *new_tree = malloc(sizeof(*new_tree));
	if (new_tree) {
		new_tree->data = stock;
		new_tree->left = new_tree->right = NULL;
		new_tree->height = 1;//Initial height
	}

	return new_tree;
}

/*Add the company stock to the tree*/
static struct tree *tree_insert(struct tree *cur_tree, struct company *stock, int (*cmp)(const struct company *, const struct company *))
{
	if (!cur_tree) {
		return tree_create(stock);
	}

	if (strcmp(stock->symbol, cur_tree->data->symbol) == 0) {
		fprintf(stderr, "Duplicate stocks!: %d\n", errno);
		stock_destroy(stock);
		return cur_tree;
	}

	if (cmp(stock, cur_tree->data) < 0) {
		cur_tree->left = tree_insert(cur_tree->left, stock, cmp);
	} else {
			cur_tree->right = tree_insert(cur_tree->right, stock, cmp);
	}

	/*	AVL Arrangement*/
	cur_tree->height = ((height(cur_tree->left) > height(cur_tree->right)) ? height(cur_tree->left) : height(cur_tree->right)) + 1;

	int balance = get_balance(cur_tree);
	int balance_child = 0;

	if (balance > 0) {
		balance_child = get_balance(cur_tree->right);
	} else {
		balance_child = get_balance(cur_tree->left);
	}
	/*	Check balance*/
	/*	Left-Left: Rotate parent left*/
	if (balance > 1 && balance_child >= 0) {
		return rot_left(cur_tree);
	}

	/*	Right-Right: Rotate parent right*/
	if (balance < -1 && balance_child < 0) {
		return rot_right(cur_tree);
	}

	/*	Left-Right: Rotate child Right, Parent Left*/
	if (balance > 1 && balance_child < 0) {
		cur_tree->right = rot_right(cur_tree->right);
		return rot_left(cur_tree); 
	}

	/*	Right-Left: Rotate child Left, Parent Right*/
	if (balance < -1 && balance_child >= 0) {
		cur_tree->left = rot_left(cur_tree->left);
		return rot_right(cur_tree);
	}
	return cur_tree;
}

bool market_insert(market *mkt, struct company *stock)
{
	if (!mkt) {
		return false;
	} else if (!mkt->root) {
		mkt->root = tree_create(stock);
		return true;
	}

/*	recursion begins in here*/
	mkt->root = tree_insert(mkt->root, stock, mkt->cmp);
	return true;
}

/*Build market from files data*/
void seed_market(market *mkt, FILE *fp)
{
	char symbol[6], name[64], *comment;
	double value = 0.0;
	memset(symbol, '\0', sizeof(symbol));
	memset(name, '\0', sizeof(name));

	while (fscanf(fp, "%s %lf %[^\n]s", symbol, &value, name) != EOF) {
		comment = strstr(name, "#");
		if (comment) {
			memset(comment, '\0', strlen(comment));
		}

		//Skip negative stock prices
		if (value < 0) {
			continue;
		}

		struct company *stock = stock_create(symbol, name, value);
		if (!stock) {
			return;
		}

		market_insert(mkt, stock);
		memset(symbol, '\0', sizeof(symbol));
		memset(name, '\0', sizeof(name));
	}
}

static bool tree_adjust(struct tree *cur_tree, struct company *stock, int (*cmp)(const struct company *, const struct company *), int operation)
{
	if (!cur_tree) {
		cur_tree = tree_create(stock);
		return true;
	}

	//Found matching symbol
	if (cmp(stock, cur_tree->data) == 0) {
		if (operation == 1) {
			if ((cur_tree->data->cents < stock->cents) ) {
				stock_destroy(stock);
				return true;
			}
			cur_tree->data->cents -= stock->cents;
			stock_destroy(stock);
			return true;
		} else {
			cur_tree->data->cents += stock->cents;
			stock_destroy(stock);
			return true;
		}

		if ((cur_tree->data->cents + stock->cents) > 0) {
			cur_tree->data->cents += stock->cents;
			stock_destroy(stock);
			return true;
		}
		//Value yields negative funds.
		stock_destroy(stock);
		fprintf(stderr, "Value deduction too Great!: %d\n", errno);
		return true;
	}
	//No match
	//Comparison = go left.
	if (cmp(stock, cur_tree->data) <= 0) {
		if(cur_tree->left) {
			return tree_adjust(cur_tree->left, stock, cmp, operation);
		}
		if (operation == 1) {
			stock_destroy(stock);
			fprintf(stderr, "Value too small!: %d\n", errno);
			return true;
		} else {
			cur_tree->left = tree_create(stock);
			return cur_tree->left;
		}
	} else { //Comparison = go right.
		if (cur_tree->right) {
			return tree_adjust(cur_tree->right, stock, cmp, operation);
		} 
		if (operation == 1) {
			stock_destroy(stock);
			fprintf(stderr, "Value too small!: %d\n", errno);
			return true;
		} else {
			cur_tree->right = tree_create(stock);
			return cur_tree->right;
		}
	}
}

/*Add/subtract user changes. Check if value becomes negative*/
void market_adjust(market *mkt, char *user_buf)
{
	if (!mkt || !user_buf) {
		return;
	}

	int operation = 0;
	char symbol[6], name[64], *comment;
	double value = 0.0;

	memset(symbol, '\0', sizeof(symbol));
	memset(name, '\0', sizeof(name));

	if (sscanf(user_buf, "%s %lf %[^\n]s", symbol, &value, name) < 2) {
		return;
	}

	if (value < 0) {
		operation = 1;
	}

	comment = strstr(name, "#");
	if (comment) {
		memset(comment, '\0', strlen(comment));
	}

	struct company *stock = stock_create(symbol, name, value);
	if (!stock) {
		return;
	}

	tree_adjust(mkt->root, stock, mkt->cmp, operation);
}

static bool build_price_tree(market *price_mkt, struct tree *tree)
{
	if (!tree) {
		return false;
	}

	if (tree->left) {
		build_price_tree(price_mkt, tree->left);
	}

	market_insert(price_mkt, tree->data);

	if (tree->right) {
		build_price_tree(price_mkt, tree->right);
	}
	return true;
}

void build_price_mkt(market *price_mkt, market *name_mkt)
{
	if (!price_mkt || !name_mkt) {
		return;
	}

	build_price_tree(price_mkt, name_mkt->root);
}

static void print_tree(struct tree *t)
{
	if (!t) {
		return;
	}

	if (t->left) {
		print_tree(t->left);
	}

	printf("%s %0.2f %s\n", t->data->symbol, t->data->cents / 100.00, t->data->name);

	if (t->right) {
		print_tree(t->right);
	}
}

void print_mkt_data(market *mkt)
{
	if (!mkt->root) {
		return;
	}

	print_tree(mkt->root);
}

/*Frees all links and data*/
void tree_destroy(struct tree *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if(cur_tree->left) {
		tree_destroy(cur_tree->left);
	}

	if(cur_tree->right) {
		tree_destroy(cur_tree->right);
	}
	stock_destroy(cur_tree->data);
	free(cur_tree);
}

/*Breaks all tree links, but data remains*/
void tree_disassemble(struct tree *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if(cur_tree->left) {
		tree_disassemble(cur_tree->left);
	}

	if(cur_tree->right) {
		tree_disassemble(cur_tree->right);
	}

	free(cur_tree);
}

void market_destroy(market *mkt, void (*dismantle)(struct tree *cur_tree))
{
	if (!mkt) {
		return;
	} else if (!mkt->root) {
		free(mkt);
	} else {
		dismantle(mkt->root);
		free(mkt);
	}
}

