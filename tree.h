
#ifndef TREE_H
#define TREE_H

#include "stock.h"

struct tree {
	struct company *data;
	struct tree *left, *right;
	int height;
};

typedef struct {
	struct tree *root;
	int (*cmp)(const struct company *, const struct company *);
} market;

market *market_create(int (*cmp)(const struct company *, const struct company *));

bool market_insert(market *mkt, struct company *stock);

void seed_market(market *mkt, FILE *fp);

void market_adjust(market *mkt, char *user_buf);

void build_price_mkt(market *price_mkt, market *name_mkt);

void print_mkt_data(market *mkt);

void tree_destroy(struct tree *cur_tree);
void tree_disassemble(struct tree *cur_tree);

void market_destroy(market *mkt, void (*dismantle)(struct tree *cur_tree));

#endif
