
#include "stock.h"

#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

/*Create the company stock*/
struct company *stock_create(char *symbol, char *name, double price)
{
	struct company *new_stock = malloc(sizeof(*new_stock));
	if (!new_stock) {
		return NULL;
	}

	new_stock->name = strdup(name);
	if (!new_stock->name) {
		free(new_stock);
		return NULL;
	}

	strncpy(new_stock->symbol, symbol, sizeof(new_stock->symbol) - 1);
	new_stock->symbol[sizeof(new_stock->symbol) - 1] = '\0';

	int index = 0;
	for (index = 0; index < (int)strlen(new_stock->symbol); index++) {
		new_stock->symbol[index] = toupper(new_stock->symbol[index]);
	}


	new_stock->cents = fabs(100 * price);

	return new_stock;
}

void stock_destroy(struct company *stock)
{
	if (stock) {
		free(stock->name);
		free(stock);
	}
	return;
}

//------Tree comparators
//Comparison function for price.
int cmp_price(const struct company *stock1, const struct company *stock2)
{
	if (!stock1 || !stock2) {
		return 0;
	}

	if (stock1->cents <= stock2->cents) {
		return -1;
	} else {
		return 1;
	}
}

//Comparison function for name.
int cmp_name(const struct company *stock1, const struct company *stock2)
{
	if (!stock1 || !stock2) {
		return 0;
	}
	
	if (strcmp(stock1->symbol, stock2->symbol) < 0) { //value less than
		return -1;
	} else if (strcmp(stock1->symbol, stock2->symbol) > 0){ //value greater than
		return 1;
	}
	return 0;
}
