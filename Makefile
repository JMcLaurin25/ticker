
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

LDLIBS+=-lm

ticker: ticker.o stock.o tree.o input.o

.PHONY: clean debug profile

clean:
	-rm *.o *.su ticker

debug: CFLAGS+=-g
debug: ticker

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: ticker
